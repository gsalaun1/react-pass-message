const {useState, useEffect} = React


const App = () => {

    const [message, setMessage] = useState(null)

    const handleMessage = (event) => {
        setMessage(event.data)
    }

    const resetMessage = () => {
        setMessage(null)
    }

    window.addEventListener("message", handleMessage);

    return (
        <section className="section">
            <div className="container">
                {!message &&
                    <div className="control is-large is-loading">
                        En attente de données
                    </div>
                }
                {message &&
                    <div>
                        <article className="panel is-primary">
                            <p className="panel-heading">
                                Message reçu
                            </p>
                            <div className="panel-block">
                                {message}
                            </div>
                        </article>
                        <button className="button is-warning" onClick={resetMessage}>Reset</button>
                    </div>
                }
            </div>
        </section>
    )
}